provider "google" {
  credentials = "${file("./creds/serviceaccount.json")}"
  project     = "stalwart-glider-309908"
  region      = "europe-west1"
}

terraform {
  backend "gcs" {
    credentials = "./creds/serviceaccount.json"
    bucket  = "terraform-remote-states29"
    prefix    = "poc/terraform.tfstate"
   }
}
